﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace DemoApp.Controllers
{
    [Route("/")]
    [ApiController]
    public class HelloController : ControllerBase
    {
        // GET /
        [HttpGet]
        public ActionResult<string> Get()
        {
            return "Hello";
        }
    }
}
