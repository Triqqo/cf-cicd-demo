FROM microsoft/dotnet:2.1-sdk-alpine as build

WORKDIR /app
COPY . .

WORKDIR /app/DemoApp
RUN dotnet restore && \
    dotnet publish -c Release -o out

FROM microsoft/dotnet:2.1-aspnetcore-runtime-alpine

WORKDIR /app
COPY --from=build /app/DemoApp/out .

ENTRYPOINT [ "dotnet", "DemoApp.dll" ]