# Cloudformation free-tier CICD demo

This repo contains a basic ECS stack on a t2.micro, with a Gitlab-runner as an ECS service. This means it fits in the AWS free tier. Obviously, this is at your own risk, so don't blame me if you do get charged!

## Deploying:

First of all, you'll need an IAM user with "Programmatic access" and the right permissions to deploy the `infra.yml` CloudFormation template. Configure your local AWS cli to use those credentials. I recommend **not** using your root user for this, but instead creating a separate user.

You might also want to modify the `InboundIPAddress` parameter to point to your specific IP address (in CIDR format, so ending with `/32`) to restrict inbound traffic. **Currently this is set to `0.0.0.0/0`, meaning anyone can access your deployed service on port 80.**

You can deploy the infra stack using the following command. Be sure to insert your gitlab runner token as the `RunnerToken` parameter.

```
aws cloudformation deploy --stack-name cicd-infra --template-file infra.yml --capabilities CAPABILITY_NAMED_IAM --parameter-overrides RunnerToken=tokenhere
```

Afterwards you could fork this repo or copy the .gitlab-ci.yml and deploy.yml files to your own project (modifying any relevant values) to run CI/CD jobs on your newly created stack.